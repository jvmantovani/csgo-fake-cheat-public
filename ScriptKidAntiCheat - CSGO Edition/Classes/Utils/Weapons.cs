﻿namespace ScriptKidAntiCheat.Utils
{
    /// <summary>
    /// Enumeration of weapon ids.
    /// </summary>
    public enum Weapons
    {
        Flashbang = 43,
        Knife = 59,
        Molotov = 46,
        Incendiary = 48,
        Grenade = 44,
        Smoke = 45,
        Scout = 40,
        Awp = 9,
        Sig = 11,
        Scar = 38,
        C4 = 49
    }
}
